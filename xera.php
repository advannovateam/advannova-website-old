<?php

namespace Advannova\WebSite\CouterPoint;

/*
 * Advannova License
 * 
 */
require_once './templates/components/slider.php';
require_once './templates/components/tab_slider.php';
require_once './src/common.php';

use Advannova\Common\Constants;
use Advannova\TemplateEngine\Templates\Components\SliderImage;

/*
 * Setting up template's configurations
 */
$view['currentPage'] = Constants::XERA_LABEL;
$view['breadcrumbs'] = [['label' => 'Solutions', 'link' => ''], ['label' => Constants::HOSPITALITY_LABEL, 'link' => Constants::HOSPITALITY_HREF]];

/*
 * Body content starts here
 */
ob_start();
//Slider Imgage
$sliderImage = new SliderImage();
$sliderImage->addSlider('img/xera/slider/1.jpeg', 'Flexibility', 'Tailor-Made for Any Establishments in the Hospitality Industry. Superior Customization Abilities to Fit Your Business Needs Like No Other.');
$sliderImage->addSlider('img/xera/slider/xera_login.jpeg', 'Efficiency', 'Lightening-Fast Operations with the Fewest Clicks Possible.');
$sliderImage->render();
?>

<div class="container-fluid gray-container">
    <div class="row margintop20">
        <div class="col-lg-5">
            <img class="align-right" src="img/xera/XERA_OrderEntry.png">
        </div>
        <div class="col-lg-7">
            <ul class="ul-features">
                <li> Easy to Learn, Use & Modify </li>
                <li> Unified Real Touch Design </li>
                <li> Flexible and Automatic Print Routing to Bar & Kitchen </li>
                <li> Flexibility on Check & Item Splitting </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <img class="align-right" src="img/xera/XERA_OrderEntry_Modifier.png">
        </div>
        <div class="col-lg-7">
            <ul class="ul-features">
                <li>Customizable Tender Keys for Faster Transactions </li>
                <li> Flexibility with Tip Pooling and Gratuity Handling </li>
                <li>Full Feature of Back Office & Built-in Reporting </li>
                <li>Integrated with Aldelo EDC Payment Solutions </li>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid small aligncenter">
    <div class="row margintop20">
        <div class="col-lg-4">
            <i class="fa fa-cogs fa-3x"></i><br>
            <h4>Efficient Operations</h4>
            <ul class="unstyled">
                <li>Flexible Bar and Kitchen Print Routing</li>
                <li>Customize Your Own Menu Panels</li>
                <li>Customizable Tender Keys for Faster Transaction Times</li>
                <li>Unlimited Modifiers for Order-Entry Accuracy</li> 
                <li>Flexible Check/Item Splitting and Combining</li> 
                <li>Easy to Modify Ordered Items in Clickable Receipts</li>
            </ul>
        </div>
        <div class="col-lg-4">
            <i class="fa fa-puzzle-piece fa-3x"></i><br>
            <h4>Item Management</h4>
            <ul class="unstyled">
                <li>Full Control Over Item Pricing and Menu Planning</li>
                <li>Monitor Inventory with Countdown Feature</li>
                <li>Substitution Prompts When the Item Runs Out</li>
            </ul>
        </div>
        <div class="col-lg-4">
            <i class="fa fa-bar-chart-o fa-3x"></i><br>
            <h4>Financial Reporting</h4>
            <ul class="unstyled">
                <li>Generate Sales, Financial & Security Analyses</li>
                <li>Customizable Reports for Each Cash Drawer, Department, Employee or Product Type</li>
                <li>End-of-Day POS Reporting</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-lg-push-2">
            <i class="fa fa-users fa-3x"></i><br>
            <h4>Workforce Management</h4>
            <ul class="unstyled">
                <li>Customizable Tip Pool and Gratuity Allocation</li>
                <li>Time Card Tracking for Payroll Management</li>
                <li>Employee Scheduling with Simple Graphical Interface</li>
                <li>Flexible Configuration of Security Access Per Employee</li> 
                <li>Automatic Enforcement of Time Card Policies as Defined By the Store</li>
            </ul>
        </div>
        <div class="col-lg-4 col-lg-push-2">
            <i class="fa fa-list fa-3x"></i><br>
            <h4>Other Features</h4>
            <ul class="unstyled">
                <li>Seating Management-Easily Attach/Detach/Transfer/Rotate Tables</li>
                <li>Extremely Flexible Security Features to Ensure Accountability</li>
                <li>Minimal Popup Screens & Boxes to Speed Up Services</li>
            </ul>
        </div>
    </div>
</div>
<!--Body content ends here-->
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
