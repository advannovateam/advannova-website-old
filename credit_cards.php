<?php

namespace Advannova\WebSite\CreditCards;

/*
 * Advannova License
 * 
 */
require_once './templates/components/tab_slider.php';

use Advannova\TemplateEngine\Templates\Components\SliderTab;

/*
 * Setting up template's configurations
 */
$view['currentPage'] = 'Credit Cards';

/*
 * Body content starts here
 */
ob_start();
?>
<div class="row marginbot40">
    <h3 class="aligncenter marginbot40"> Credit Card Services</h3>
    <div class="col-lg-4 center-block col-lg-push-1">
        <img class="img-responsive center-block" src="img/credit_cards/credit-cards.jpg">
        <p class="text-center">
            At Advannova, we pride ourselves in being able to offer our customers various solutions to
            fit their ideal business needs. Below are the different credit card processors for whom we are brokers.
        </p>
    </div>
    <div class="col-lg-8 col-lg-push-1">
        <img class="img-responsive center-block" src="img/credit_cards/customers_satisfaction.jpg">
    </div>
</div>
<div class="container">
    <?php
//Jssor tab slider
    $sliderTab = new SliderTab();
    $sliderTab->addSliderTab('html_inner_pages/credit_cards_pages/ccchase.html', 'Chase Paymentech');
    $sliderTab->addSliderTab('html_inner_pages/credit_cards_pages/ccfdat.html', 'First Data');
    $sliderTab->addSliderTab('html_inner_pages/credit_cards_pages/ccmerc.html', 'Mercury');
    $sliderTab->addSliderTab('html_inner_pages/credit_cards_pages/ccsterling.html', 'Sterling');
    $sliderTab->addSliderTab('html_inner_pages/credit_cards_pages/cctsys.html', 'TSYS');
    $sliderTab->addSliderTab('html_inner_pages/credit_cards_pages/ccvant.html', 'Vantiv');
    $sliderTab->addSliderTab('html_inner_pages/credit_cards_pages/ccwpay.html', 'WorldPay');
    $sliderTab->render($view, 'js/tab_slider_jssor/credit_card_tab_slider.js');
    ?>
</div>
<?php
/*
 * Body content ends here
 */
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
