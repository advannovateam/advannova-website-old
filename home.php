<?php

namespace Advannova\WebSite\Home;

/*
 * Advannova License
 * 
 */
require_once __DIR__ . '/src/common.php';
require_once './templates/components/slider.php';
require_once './templates/components/image_boxes.php';

use Advannova\Common\Constants;
use Advannova\TemplateEngine\Templates\Components\SliderImage;
use Advannova\TemplateEngine\Templates\Components\ImageBoxes;

/*
 * Setting up template's configurations
 */
$view['currentPage'] = 'Home';

ob_start();
?>

<div id="wrapper">
    <section id="featured">
        <?php
        $slider = new SliderImage();
        $slider->addSlider('img/home/slider/counterpoint_xr7.jpg', 'Counterpoint POS', "NCR Counterpoint includes robust point-of-sale, inventory management software, built-in customer loyalty, automated purchasing, and configurable reporting capabilities.", ['text' => 'Learn more', 'link' => Constants::COUNTERPOINT_HREF]);
        $slider->addSlider('img/home/slider/counterpoint_mobile.jpg', 'Counterpoint Mobile', 'Run your business from the sales floor as you help customers find products and ring up sales right on the spot. Mobility also allows you to sell your products wherever your customers are gathering: events, sidewalk sales, and trade shows.', ['text' => 'Learn more', 'link' => Constants::COUNTERPOINT_HREF]);
        $slider->addSlider('img/home/slider/ncr-xr7.jpg', 'NCR Hardware', 'With their bright, graphical touchscreens and transaction displays, our mobile and fixed POS terminals help your staff deliver exceptional service. ', ['text' => 'Learn more', 'link' => Constants::COUNTERPOINT_HREF]);
        $slider->addSlider('img/home/slider/software_development.jpg', 'Software Development', "Whatever you can imagine, we can develop it, to help you run your business efficiently and profitably.", ['text' => 'Learn more', 'link' => Constants::SOFTWARE_DEVELPMENT_HREF]);
        $slider->addSlider('img/home/slider/trucks.jpg', 'Limitless Logistix', "GPS made easy!  Whether you require a portable tracker for short term use or installed units for your fleet of service trucks, you've come to the right place!", ['text' => 'Learn more', 'link' => Constants::LIMITLESS_LOGISTIX_HREF]);
        $slider->render("hidden-xs hidden-sm");
        ?>
    </section>
    <section class="callaction">
        <div class="container">
            <div class="cta-text hidden-xs">
                <h2>Using <span>Advan</span>ced In<span>nova</span>tions  to Turn Data into Knowledge</h2>
            </div>
            <!--Slogan for small devices-->
            <div class="cta-text visible-xs">
                <h4>Using <span>Advan</span>ced In<span>nova</span>tions  to Turn Data into Knowledge</h4>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <?php
            $imageBoxes = new ImageBoxes();
            $imageBoxes->addBox("img/home/ncr-xr7.jpg", Constants::RETAIL_POS_LABEL, Constants::RETAIL_POS_HREF);
            $imageBoxes->addBox("img/home/rest_manager.png", Constants::HOSPITALITY_LABEL, Constants::HOSPITALITY_HREF);
            $imageBoxes->addBox("img/home/credit_cards-hand.png", Constants::CREDIT_CARDS_LABEL, Constants::CREDIT_CARDS_HREF);
            $imageBoxes->addBox("img/home/workflow-development.jpg", Constants::SOFTWARE_DEVELPMENT_LABEL, Constants::SOFTWARE_DEVELPMENT_HREF);
            $imageBoxes->addBox("img/home/moving-truck.jpg", Constants::LIMITLESS_LOGISTIX_LABEL, Constants::LIMITLESS_LOGISTIX_HREF);
            $imageBoxes->addBox("img/home/support.jpg", Constants::SUPPORT_LABEL, Constants::SUPPORT_HREF);
            $imageBoxes->render(2, 3);
            ?>
        </div>
    </section>
</div>
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_header_footer.php';
