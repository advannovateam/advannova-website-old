<?php

namespace Advannova\WebSite\Support;

/*
 * Advannova License
 * 
 */
/*
 * Setting up template's configurations
 */
require_once './src/common.php';

use Advannova\Common\Constants;

$view['currentPage'] = Constants::SUPPORT_TICKET_LABEL;
$view['breadcrumbs'] = [['label' => Constants::SUPPORT_LABEL, 'link' => Constants::SUPPORT_HREF]];
ob_start();
?>
<div class="container margintop30 marginbot30">

    <div class="aligncenter ">
        <h2><?php echo Constants::SUPPORT_TICKET_LABEL; ?></h2>
        <img class="center-block" src="img/support/create-ticket.png">
    </div>
    <!--  ----------------------------------------------------------------------  -->
    <!--  NOTE: Please add the following <FORM> element to your page.             -->
    <!--  ----------------------------------------------------------------------  -->

    <form class="center-block" id="form-create-ticket" action="https://www.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8" method="POST">

        <input type=hidden name="orgid" value="00D36000000HD9W">
        <input type=hidden name="retURL" value="http://advannova.com/advannova_ws/current/support.php">

        <!--  ----------------------------------------------------------------------  -->
        <!--  NOTE: These fields are optional debugging elements. Please uncomment    -->
        <!--  these lines if you wish to test in debug mode.                          -->
        <!--  <input type="hidden" name="debug" value=1>                              -->
        <!--  <input type="hidden" name="debugEmail" value="linda@advannova.com">     -->
        <!--  ----------------------------------------------------------------------  -->
        <div class="form-group">
            <label for="company">Company</label>
            <input class="form-control"  id="company" maxlength="80" name="company" size="20" type="text" />
        </div>

        <div class="form-group">
            <label for="name">Contact Name</label>
            <input class="form-control"  id="name" maxlength="80" name="name" size="20" type="text" />
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" id="email" maxlength="80" name="email" size="20" type="email" />
        </div>

        <div class="form-group">
            <label for="phone">Phone</label>
            <input class="form-control"  id="phone" maxlength="40" name="phone" size="20" type="tel" />
        </div>

        <div class="form-group">
            <label for="subject">Subject</label>
            <input class="form-control"  id="subject" maxlength="80" name="subject" size="20" type="text" />
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" name="description"></textarea>
        </div>

        <div class="form-group">
            <label for="type">Type</label>
            <select class="form-control" id="type" name="type"><option value="">--None--</option><option value="Problem">Problem</option>
                <option value="Feature Request">Feature Request</option>
                <option value="Question">Question</option>
            </select>
        </div>

        <div class="form-group">
            <label for="reason">Case Reason</label>
            <select class="form-control"  id="reason" name="reason"><option value="">--None--</option><option value="User didn&#39;t attend training">User didn&#39;t attend training</option>
                <option value="Complex functionality">Complex functionality</option>
                <option value="Existing problem">Existing problem</option>
                <option value="Instructions not clear">Instructions not clear</option>
                <option value="New problem">New problem</option>
            </select>
        </div>

        <div class="form-group">
            <label for="status">Status</label>
            <select class="form-control" id="status" name="status"><option value="">--None--</option><option value="On Hold">On Hold</option>
                <option value="Escalated">Escalated</option>
                <option value="Closed">Closed</option>
                <option value="New">New</option>
            </select>
        </div>

        <div class="form-group">
            <label for="priority">Priority</label>
            <select class="form-control" id="priority" name="priority"><option value="">--None--</option><option value="High">High</option>
                <option value="Medium">Medium</option>
                <option value="Low">Low</option>
            </select>
        </div>

        <div class="form-group">
            <label for="00N36000000jT8j">Alert Message</label>
            <textarea class="form-control" id="00N36000000jT8j" name="00N36000000jT8j" type="text" wrap="soft"></textarea>
        </div>
        <input class="btn btn-primary center-block" type="submit" name="submit">
    </form>
</div>
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
