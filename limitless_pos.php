<?php

namespace Advannova\WebSite\Retail\Limitless;

/*
 * Advannova License
 * 
 */
require_once './src/common.php';
require_once './templates/components/slider.php';
require_once './templates/components/tab_slider.php';

use Advannova\Common\Constants;
use Advannova\TemplateEngine\Templates\Components\SliderImage;
use Advannova\TemplateEngine\Templates\Components\SliderTab;

/*
 * Setting up template's configurations
 */
$view['currentPage'] = Constants::LIMITLESS_POS_LABEL;
$view['breadcrumbs'] = [['label' => 'Solutions', 'link' => ''], ['label' => Constants::RETAIL_POS_LABEL, 'link' => Constants::RETAIL_POS_HREF]];

ob_start();
//Slider Imgage
$sliderImage = new SliderImage();
$sliderImage->addSlider('img/limitless_pos/slider/limitless_screen_shut.jpg', '', '');
$sliderImage->addSlider('img/limitless_pos/slider/limitless_screen_shut_1.jpg', '', '');
$sliderImage->addSlider('img/limitless_pos/slider/limitless_screen_shut_2.jpg', '', '');
$sliderImage->render();
?>

<div class="container">
    <?php
//Jssor tab slider
    $sliderTab = new SliderTab();
    $sliderTab->addSliderTab('html_inner_pages/limitless_pos/features.html', 'Features');
    $sliderTab->addSliderTab('html_inner_pages/limitless_pos/inventory_control.html', 'Inventory Control');
    $sliderTab->addSliderTab('html_inner_pages/limitless_pos/product_pricing.html', 'Product Pricing');
    $sliderTab->addSliderTab('html_inner_pages/limitless_pos/customer_tracking.html', 'Customer Tracking');
    $sliderTab->render($view, 'js/tab_slider_jssor/hospitality_tab_slider.js');
    ?>
</div>
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
