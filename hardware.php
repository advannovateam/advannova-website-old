<?php

namespace Advannova\WebSite\Retail;

/*
 * Advannova License
 * 
 */
/*
 * Setting up template's configurations
 */
require_once './src/common.php';

use Advannova\Common\Constants;

$view['currentPage'] = Constants::HARDWARE_LABEL;
$view['breadcrumbs'] = [['label' => 'Solutions', 'link' => '']];

ob_start();
?>
<div class="container margintop30">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-2">
            <a href="<?php echo Constants::HARDWARE_HREF; ?>">
                <img class="img-responsive center-block" src="img/hardware/ncr/ncr-xr7-hardware.png">
                <h4 class="text-center"><?php echo Constants::NCR_HARDWARE_LABEL; ?></h4>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="<?php echo Constants::HARDWARE_HREF; ?>">
                <img class="img-responsive center-block" src="img/hardware/touch_dynamic/touch_dynamic_hardware.png">
                <h4 class="text-center"><?php echo Constants::TOUCH_DYNAMIC_LABEL; ?></h4>
            </a>
        </div>
    </div>
</div>
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
