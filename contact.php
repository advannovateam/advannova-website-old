<?php

namespace Advannova\WebSite\Contact;

/*
 * Advannova License
 * 
 */
/*
 * Setting up template's configurations
 */
require_once './src/common.php';

use Advannova\Common\Constants;

$view['currentPage'] = 'Contact';

/*
 * Body content starts here
 */
ob_start();
?>
<div class="container margintop30">
    <div class="row">
        <div class="col-lg-6">
            <div class="row">
                <h4><i class="fa fa-envelope"></i> Get in touch with us by filling <strong>contact form below</strong></h4>
                <form id="contactform" action="<?php echo Constants::EMAIL_DELIVER_HREF; ?>" method="post" class="validateform" name="send-contact">
                    <div id="sendmessage">
                        Your message has been sent. Thank you!
                    </div>
                    <div class="row">
                        <div class="col-lg-4 field">
                            <input type="text" name="name" placeholder="* Enter your full name" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validation">
                            </div>
                        </div>
                        <div class="col-lg-4 field">
                            <input type="text" name="email" placeholder="* Enter your email address" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validation">
                            </div>
                        </div>
                        <div class="col-lg-4 field">
                            <input type="text" name="subject" placeholder="Enter your subject" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validation">
                            </div>
                        </div>
                        <div class="col-lg-12 margintop10 field">
                            <textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." data-rule="required" data-msg="Please write something"></textarea>
                            <div class="validation">
                            </div>
                            <p>
                                <button class="btn btn-theme margintop10 pull-left" type="submit">Submit message</button>
                                <span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-6">
            <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3436.8978020992245!2d-97.64351088545793!3d30.52392880271798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8644d05ae77d592b%3A0x558df5d7d6ed22fa!2s2851+Joe+Dimaggio+Blvd+%2329%2C+Round+Rock%2C+TX+78665!5e0!3m2!1sen!2sus!4v1452532330179" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
            <iframe style="width: 100%; height: 440px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3436.8978020992245!2d-97.64351088545793!3d30.52392880271798!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8644d05ae77d592b%3A0x558df5d7d6ed22fa!2s2851+Joe+Dimaggio+Blvd+%2329%2C+Round+Rock%2C+TX+78665!5e0!3m2!1sen!2sus!4v1452532330179">
            </iframe>
        </div>
    </div>
</div>
<!--Body content ends here-->
<?php
$view['body_content'] = ob_get_clean();
$view['html_js_src'][] = 'js/validate.js';
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
