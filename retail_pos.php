<?php

namespace Advannova\WebSite\Retail;

/*
 * Advannova License
 * 
 */
/*
 * Setting up template's configurations
 */
require_once './src/common.php';

use Advannova\Common\Constants;

$view['currentPage'] = Constants::RETAIL_POS_LABEL;
$view['breadcrumbs'] = [['label' => 'Solutions', 'link' => '']];

ob_start();
?>
<div class="row">
    <h3 class="text-center"><?php echo Constants::RETAIL_POS_LABEL; ?> Solutions</h3>
    <div class="col-lg-6">
        <a href="<?php echo Constants::COUNTERPOINT_HREF; ?>">
            <img src="img/retail_pos/ncr-xr7.jpg" class="img-responsive center-block">
            <h5 class="text-center"><?php echo Constants::COUNTERPOINT_LABEL; ?></h5>
        </a>
        <p style="padding-right: 20px; padding-left: 20px;" class="text-justify"><strong>Complete Retail Management Solution</strong>. NCR Counterpoint includes robust point-of-sale, inventory management softwareComplete Retail Management Solution, built-in customer loyalty, automated purchasing, and configurable reporting capabilities.</p>
    </div>
    <div class="col-lg-6">
        <a href="<?php echo Constants::LIMITLESS_POS_HREF; ?>">
            <img src="img/retail_pos/limitless-pos.png" class="img-responsive center-block">
            <h5 class="text-center"><?php echo Constants::LIMITLESS_POS_LABEL; ?></h5>
        </a>
        <p style="padding-right: 20px; padding-left: 20px;" class="text-justify">Limitless POS is an <strong>easy</strong> to use, <strong>easy</strong> to setup, <strong>easy</strong> to maintain POS for the small to medium Retail and/or Quick Service business.</p>
    </div>
</div>
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
