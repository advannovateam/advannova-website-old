<?php

namespace Advannova\WebSite\Contact;

/*
 * Advannova License
 * 
 */
/*
 * Setting up template's configurations
 */
require_once './src/common.php';

use Advannova\Common\Constants;

$view['currentPage'] = Constants::FINANCING_LABEL;

/*
 * Body content starts here
 */
ob_start();
?>
<div class="container margintop20 marginbot20">
    <div class="col-lg-6">
        <h5 class="text-center"><?php echo Constants::FINANCING_ASCENTIUM_LABEL; ?></h5>
        <img src="img/financing/ascentium_capital.png" class="img-responsive center-block">
        <p class="text-justify">
            Ascentium Capital is a leading provider of equipment and technology financing solutions. Advannova has found
            Ascentium’s unique finance platform, combined with exceptional customer service, paves the way by providing our
            customers fast, flexible financing which serves small, mid-sized and Fortune 500 companies. Business owners know
            that time, like money, is a prized commodity. Ascentium Capital will save you time and get you the money you need
            to capitalize on all your business opportunities.
        </p>
        <ul class="unstyled">
            Get quoted and apply for a loan:
            <li class="margintop10"><a class="btn btn-info" href="<?php echo Constants::FINANCING_LESS_2_YEARS_HREF; ?>" target="_blank"><?php echo Constants::FINANCING_LESS_2_YEARS_LABEL; ?></a></li>
            <li class="margintop10"><a class="btn btn-info" href="<?php echo Constants::FINANCING_PLUS_2_YEARS_HREF; ?>" target="_blank"><?php echo Constants::FINANCING_PLUS_2_YEARS_LABEL; ?></a></li>
        </ul>
    </div>
    <div class="col-lg-6">
        <h5 class="text-center"><?php echo Constants::FINANCING_VEND_LEASE_LABEL; ?></h5>
        <img src="img/financing/vend_lease.png" class="img-responsive center-block">            
        <p class="text-justify">
            We at Advannova, place trust in Vend Lease to handle the financing needs of our customers and count on Vend Lease 
            to consistently deliver the highest level of service and value within the point of sale industry. As a direct funding
            source, Vend Lease is well positioned to remain steadfast to the markets we serve by creating flexible programs and 
            cost-effective solutions to business owners that require commercial equipment. Vend Lease is the right choice to secure 
            financing, designed specifically to meet the individual needs of your business.
        </p>
        <div class="text-center">
            <a class="btn btn-info" href="<?php echo Constants::FINANCING_VEND_LEASE_HREF; ?>">Apply</a>
        </div>

    </div>
</div>
<!--Body content ends here-->
<?php
$view['body_content'] = ob_get_clean();
$view['html_js_src'][] = 'js/validate.js';
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
