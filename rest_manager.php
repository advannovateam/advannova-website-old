<?php

namespace Advannova\WebSite\CouterPoint;

/*
 * Advannova License
 * 
 */
require_once './templates/components/slider.php';
require_once './templates/components/tab_slider.php';
require_once './src/common.php';

use Advannova\Common\Constants;
use Advannova\TemplateEngine\Templates\Components\SliderTab;
/*
 * Setting up template's configurations
 */
$view['currentPage'] = Constants::REST_MANAGER_LABEL;
$view['breadcrumbs'] = [['label' => 'Solutions', 'link' => ''], ['label' => Constants::HOSPITALITY_LABEL, 'link' => Constants::HOSPITALITY_HREF]];

ob_start();
?>

<img class="img-responsive" src="img/rest_manager/rest_manager.jpg"

<!--FEATURES-->
<div class="blue-container">
    <div class="row margintop20">
        <div class="col-lg-6">
            <img class="align-right" src="img/rest_manager/ipad_rm.png">
        </div>
        <div class="col-lg-6">
            <ul class="ul-features">
                <li> Easy to Install, Use, and Maintain </li>
                <li> Fast and Simple Order Entry </li>
                <li> Process Credit, Debit, and Gift Cards Including Tips </li>
                <li> Table Management </li>
                <li> Easy to Install, Use, and Maintain </li>
                <li> Fast and Simple Order Entry </li>
                <li> Process Credit, Debit, and Gift Cards Including Tips </li>
                <li> Table Management </li>
            </ul>
        </div>
    </div>
</div>
<!--END OF FEATURES-->

<!--AREAS-->
<div class="container margintop40 marginbot30">
    <?php
    //Jssor tab slider
    $sliderTab = new SliderTab();
    $sliderTab->addSliderTab('html_inner_pages/rest_manager/table_service.html', 'Table Service');
    $sliderTab->addSliderTab('html_inner_pages/rest_manager/bars_nightclubs.html', 'Bars &amp; Nightclubs');
    $sliderTab->addSliderTab('html_inner_pages/rest_manager/takeout_delivery.html', 'Takeout &amp; Delivery');
    $sliderTab->addSliderTab('html_inner_pages/rest_manager/quick_service.html', 'Quick Service');
    $sliderTab->render($view, 'js/tab_slider_jssor/counterpoint_tab_slider.js');
    ?>
</div>
<!--END OF AREAS-->

<!--Body content ends here-->
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
