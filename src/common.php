<?php

/*
 * Advannova License
 * 
 */

namespace Advannova\Common;

class Constants {

    const CONTACT_US_LABEL = "Contact";
    const CONTACT_US_HREF = "contact.php";
    const COUNTERPOINT_LABEL = "NCR Counterpoint";
    const COUNTERPOINT_HREF = "counterpoint.php";
    const CREDIT_CARDS_LABEL = "Credit Cards";
    const CREDIT_CARDS_HREF = "credit_cards.php";
    const FINANCING_LABEL = "Financing";
    const FINANCING_HREF = "financing.php";
    const FINANCING_PLUS_2_YEARS_LABEL = "Existing Business 2+ years";
    const FINANCING_PLUS_2_YEARS_HREF = "https://icalcpayment.com/Default.aspx?primary=0a36759b-9dc6-493f-a740-b004a0229ba3";
    const FINANCING_LESS_2_YEARS_LABEL = "Start-Up Business < 2 years";
    const FINANCING_LESS_2_YEARS_HREF = "https://icalcpayment.com/Default.aspx?primary=b3d94da8-7607-4031-a747-0c61e2496783";
    const FINANCING_LESS_2_YEARS_HREF_IFRAME = "financing.php?type=less";
    const FINANCING_ASCENTIUM_LABEL = "Ascentium Capital";
    const FINANCING_VEND_LEASE_LABEL = "Vend Lease Company";
    const FINANCING_VEND_LEASE_HREF = "vend_lease_company.php";
    const EMAIL_DELIVER_HREF = "src/contact/deliver_email.php";
    const EMAIL_DELIVER_ADDRESS = "support@advannova.com";
    const HARDWARE_LABEL = "Hardware";
    const HARDWARE_HREF = "hardware.php";
    const HOSPITALITY_LABEL = "Hospitality POS";
    const HOSPITALITY_HREF = "hospitality_pos.php";
    const LIMITLESS_LOGISTIX_LABEL = "Logistix";
    const LIMITLESS_LOGISTIX_HREF = "http://limitlesslogistix.com";
    const LIMITLESS_POS_LABEL = "Limitless POS";
    const LIMITLESS_POS_HREF = "limitless_pos.php";
    const NCR_HARDWARE_LABEL = "NCR";
    const NCR_HARDWARE_HREF = "ncr_hardware.php";
    const REST_MANAGER_LABEL = "Restaurant Manager";
    const REST_MANAGER_HREF = "rest_manager.php";
    const PIONEER_LABEL = "PIONEERPOS";
    const PIONEER_HREF = "pioneerpos_hardware.php";
    const RETAIL_POS_LABEL = "Retail POS";
    const RETAIL_POS_HREF = "retail_pos.php";
    const SUPPORT_LABEL = "Support";
    const SUPPORT_HREF = "support.php";
    const SUPPORT_TICKET_LABEL = "Create a Support Request";
    const SUPPORT_TICKET_HREF = "support_ticket.php";
    const TOUCH_DYNAMIC_LABEL = "TOUCH DYNAMIC";
    const TOUCH_DYNAMIC_HREF = "touch_dynamic_hardware.php";
    const SOFTWARE_DEVELPMENT_LABEL = "Software Development";
    const SOFTWARE_DEVELPMENT_HREF = "software_development.php";
}
