<?php

namespace Advannova\WebSite\Contact;

require_once __DIR__ . '/../common.php';

use Advannova\Common\Constants;

error_reporting(E_ALL ^ E_NOTICE);

$post = (!empty($_POST)) ? true : false;

if ($post) {

    $name = stripslashes($_POST['name']);
    $email = trim($_POST['email']);
    $subject = stripslashes($_POST['subject']);
    $message = "<html><body>"
            . stripslashes($_POST['message']) 
            . "<p><small>This email was sent from <a href='http://advannova.com'>www.advannova.com</a></p></small>"
            . "</body></html>";

    $headers = "MIME-Version: 1.0" . PHP_EOL
            . "Content-type: text/html" . PHP_EOL
            . "From: " . $name . " <" . $email . ">" . PHP_EOL
            . "Reply-To: " . $email . "" . PHP_EOL
            . "X-Mailer: PHP/" . phpversion();

    $error = '';

    if (!$error) {
        $mail = mail(Constants::EMAIL_DELIVER_ADDRESS, $subject, $message, $headers);

        if ($mail) {
            echo 'OK';
        } else
            echo 'Not working';
    }
}