<?php

namespace Advannova\WebSite\Hospitality;

/*
 * Advannova License
 * 
 */
/*
 * Setting up template's configurations
 */
require_once './src/common.php';

use Advannova\Common\Constants;

$view['currentPage'] = Constants::HOSPITALITY_LABEL;
$view['breadcrumbs'] = [['label' => 'Solutions', 'link' => '']];

ob_start();
?>
<div class="row">
    <h3 class="text-center"><?php echo Constants::HOSPITALITY_LABEL; ?> Solutions</h3>
    <div class="col-lg-3 col-lg-offset-2">
        <a href="<?php echo Constants::REST_MANAGER_HREF; ?>">
            <img src="img/hospitality/rest_manager_pos.png" class="img-responsive center-block">
            <h5 class="text-center"><?php echo Constants::REST_MANAGER_LABEL; ?></h5>
        </a>
    </div>
    <div class="col-lg-3 col-lg-offset-2">
        <a href="<?php echo Constants::LIMITLESS_POS_HREF; ?>">
            <img src="img/hospitality/limitless-pos.png" class="img-responsive center-block">
            <h5 class="text-center"><?php echo Constants::LIMITLESS_POS_LABEL; ?></h5>
        </a>
    </div>
</div>
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
