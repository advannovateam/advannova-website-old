<?php

namespace Advannova\WebSite\Contact;

/*
 * Advannova License
 * 
 */
/*
 * Setting up template's configurations
 */
require_once './src/common.php';

use Advannova\Common\Constants;

$view['currentPage'] = Constants::FINANCING_VEND_LEASE_LABEL;
$view['breadcrumbs'] = [['label' => Constants::FINANCING_LABEL, 'link' => Constants::FINANCING_HREF]];

/*
 * Body content starts here
 */
ob_start();
?>
<div class="text-center">
<iframe
       src="https://www.elbtools.com/secure/apply.php?elbt=1476192164170"
       width="750"
       height="750"
       scrolling="yes"
       frameborder="0"
></iframe>
</div>

<!--Body content ends here-->
<?php
$view['body_content'] = ob_get_clean();
$view['html_js_src'][] = 'js/validate.js';
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
