<?php

namespace Advannova\WebSite\CreditCards;

/*
 * Advannova License
 * 
 */
require_once './templates/components/tab_slider.php';
require_once './src/common.php';

use Advannova\TemplateEngine\Templates\Components\SliderTab;
use Advannova\Common\Constants;

/*
 * Setting up template's configurations
 */
$view['currentPage'] = Constants::SOFTWARE_DEVELPMENT_LABEL;

/*
 * Body content starts here
 */
ob_start();
?>
<div class="row marginbot40">
    <h3 class="aligncenter marginbot40"> Software Development</h3>
    <div class="center-block">
        <img class="img-responsive center-block" src="img/software_development/custom _dev.png" alt="Software Development">
    </div>    
</div>
<h4 class="text-center">
        Our software development team is comprised of 6 talented members with a total of 67+ years of experience. 
</h4>

<div class="container">
    <?php
//Jssor tab slider
    $sliderTab = new SliderTab();
    $sliderTab->addSliderTab('html_inner_pages/software_development/custom_software.html', 'Custom Software');
    $sliderTab->addSliderTab('html_inner_pages/software_development/web_development.html', 'Web Development');
    $sliderTab->addSliderTab('html_inner_pages/software_development/ecommerce.html', 'E-Commerce');
    $sliderTab->addSliderTab('html_inner_pages/software_development/mobile_app.html', 'Mobile App');
    $sliderTab->addSliderTab('html_inner_pages/software_development/business_intelligence.html', 'Business Intelligence');
    $sliderTab->addSliderTab('html_inner_pages/software_development/seo.html', 'SEO');
    $sliderTab->render($view, 'js/tab_slider_jssor/credit_card_tab_slider.js');
    ?>
</div>

<!--
<div class="container text-center" style="font-size: large; font-weight: bold">
    
    <div class="col-lg-6">
        <h4 class="text-center">Services:</h4>
    <ul class="list-unstyled">
        <li>Custom Software</li>
        <li>Web Development</li>
        <li>Ecommerce Website</li>
        <li>Hosting</li>
        <li>Mobile App</li>
        <li>Integrations</li>
        <li>Enterprise</li>
        <li>Business Intelligence</li>
        <li>SEO</li>
    </ul>
        </div>
    
    <div class="col-lg-6">
        <h4 class="text-center">Technologies:</h4>
     <ul class="list-unstyled">
        <li>Java Development</li>        
        <li>C# Development</li>
        <li>C++ Development</li>
        <li>.Net Development</li>
        <li>Python</li>
        <li>PHP Development</li>
        <li>Business Analytics</li>
        <li>Saas and Cloud Development</li>
        <li>Big Data</li>
    </ul>   
     </div>
    -->
    
<?php
/*
 * Body content ends here
 */
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
