<!DOCTYPE html>
<!--
Advannova License
-->
<html lang="en">
    <head>
        <meta  HTTP-EQUIV="Content-type" CONTENT="text/html; charset=utf-8">
        <title>Advannova - Using Advanced Innovations</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />

        <link rel="shortcut icon" href="img/logo-favicon.png">

        <!-- css -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
        <link href="css/flexslider.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="css/advannova.css" rel="stylesheet" />
        <!-- Theme skin -->
        <link href="css/skins/default.css" rel="stylesheet" />

        <?php
        if (isset($view['html_css_links'])) {
            foreach ($view['html_css_links'] as $link) {
                ?>
                <link href="<?php echo $link ?>" rel="stylesheet" />
                <?php
            }
        }
        ?>
    </head>
    <body>

        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5NLM2D"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-5NLM2D');</script>
        <!-- End Google Tag Manager -->

        <!--Start of HappyFox Live Chat Script-->
        <script>
            window.HFCHAT_CONFIG = {
                EMBED_TOKEN: "c157ef00-4c23-11e5-8997-2d641ac8b507",
                ACCESS_TOKEN: "798cbcf3f5e149f3a70586849819d7cc",
                HOST_URL: "https://www.happyfoxchat.com",
                ASSETS_URL: "https://d1l7z5ofrj6ab8.cloudfront.net/visitor"
            };

            (function () {
                var scriptTag = document.createElement('script');
                scriptTag.type = 'text/javascript';
                scriptTag.async = true;
                scriptTag.src = window.HFCHAT_CONFIG.ASSETS_URL + '/js/widget-loader.js';

                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(scriptTag, s);
            })();
        </script>
        <!--End of HappyFox Live Chat Script-->


        <?php
        /*
         * All pages which implement this tamplate must have a variable
         * $view['body']
         * to print their content in the body section. 
         */
        echo $view['body']
        ?>

        <!-- javascript
                    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/google-code-prettify/prettify.js"></script>
        <script src="js/jquery.flexslider.js"></script>
        <script src="js/custom.js"></script>

        <?php
        //Javascript sources
        if (isset($view['html_js_src'])) {
            foreach ($view['html_js_src'] as $link) {
                ?>
                <script src="<?php echo $link ?>"></script>
                <?php
            }
        }
        ?>
        <!--Javascripts inpage codes-->
        <?php
        if (isset($view['js_script'])) {
            foreach ($view['js_script'] as $script) {
                echo $script;
            }
        }
        ?>
    </body>
</html>

