<?php

namespace Advannova\TemplateEngine\Templates\Components;

/*
 * Advannova License
 * 
 */

class ImageBoxes {

    private $titles, $srcImages, $hrefs;

    function __construct() {
        $this->titles = [];
        $this->srcImages = [];
        $this->hrefs = [];
    }

    public function addBox($srcImage, $title = null, $hrefs = null) {
        $this->titles[] = $title;
        $this->srcImages[] = $srcImage;
        $this->hrefs[] = $hrefs;
    }

    public function render($countRows = 1, $countCol = 4) {
        for ($row = 0; $row < $countRows; $row++):
            ?>
            <div class="row">
                <?php
                for ($col = 0; ($col < $countCol); $col++):
                    ?>   
                    <?php
                    $index = $row * $countCol + $col;
                    if (($row + 1 == $countRows) && ($index == count($this->srcImages)))
                        return;
                    ?>
                    <div class="col-sm-<?php echo 12 / $countCol ?> aligncenter">
                        <div class="box">
                            <div>
                                <a href="<?php echo $this->hrefs[$index]; ?>">
                                    <img class="img-responsive center-block" src="<?php echo $this->srcImages[$index]; ?>">
                                    <?php if ($this->titles[$index] != null): //If you only wnat to show the image do not put title in the array?>
                                        <div class="box-bottom center-block">
                                            <span><?php echo $this->titles[$index]; ?></span>
                                        </div>
                                    <?php endif; ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
            <?php
        endfor;
    }

}
