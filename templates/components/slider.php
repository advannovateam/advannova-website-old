<?php

namespace Advannova\TemplateEngine\Templates\Components;

/*
 * Advannova License
 * 
 */

class SliderImage {

    private $titles, $srcImages, $summariesText, $learnMore;

    function __construct() {
        $this->titles = [];
        $this->srcImages = [];
        $this->summariesText = [];
    }

    /*
     * The parameter learnmore has the next format
     * $learnMore = ['text' => 'Learn more', 'link' => '#']
     */

    public function addSlider($srcImage, $title = null, $summaryText = null, $learnMore = null) {
        $this->titles[] = $title;
        $this->srcImages[] = $srcImage;
        $this->summariesText[] = $summaryText;
        $this->learnMore[] = $learnMore;
    }

    public function render($cssClasses = null) {
        ?>
        <!-- Slider -->
        <div id="main-slider" class="flexslider <?php echo $cssClasses; ?>">
            <ul class="slides">
                <?php
                for ($index = 0; $index < count($this->srcImages); $index++) {
                    ?>
                    <li>
                        <img src="<?php echo $this->srcImages[$index]; ?>" class="img-responsive" alt="" />
            <?php if ($this->titles[$index] != null && $this->summariesText[$index] != null): ?>
                            <div class="flex-caption">
                                <h3><?php echo $this->titles[$index]; ?></h3> 
                                <p><?php echo $this->summariesText[$index]; ?></p> 
                <?php if (isset($this->learnMore[$index])): ?>
                                    <a href="<?php echo $this->learnMore[$index]['link']; ?>" class="btn btn-theme"><?php echo $this->learnMore[$index]['text']; ?></a>
                                </div>
                            <?php endif;
                        endif;
                        ?>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <?php
    }

}
