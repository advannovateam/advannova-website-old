<?php

namespace Advannova\TemplateEngine\Templates\Components;

/*
 * Advannova License
 * 
 */

class SliderTab {

    private $tabsText, $srcContents;

    function __construct() {
        $this->tabsText = [];
        $this->srcContents = [];
    }

    public function addSliderTab($srcContent, $tabText) {
        $this->srcContents[] = $srcContent;
        $this->tabsText[] = $tabText;
    }

    private function renderSlideDivContent($srcContent) {
        ?>
        <div style="margin: 10px 10px 10px 80px; height: 550px; overflow: hidden; overflow-y: scroll; color: #000;"><div><?php include $srcContent; ?></div></div>
        <?php
    }

    private function renderSlideDivButton($tabText) {
        ?>
        <div u="thumb"><p><?php echo $tabText; ?></p></div>
        <?php
    }

    private function renderSlideElement($tabText, $srcContent) {
        ?>
        <div>
            <?php
            $this->renderSlideDivContent($srcContent);
            $this->renderSlideDivButton($tabText);
            ?>
        </div>
        <?php
    }

    /*
     * The view reference is to be able to add the file references to the current view
     */
    public function render(&$viewReference, $jsSrcConfig) {
        /*
         * Adding dependencies (Js and CSS)
         */
        $viewReference['html_js_src'] = ['js/tab_slider_jssor/jssor.slider.min.js',$jsSrcConfig];
        $viewReference['html_css_links'] = ['css/tab_slider_jssor.css'];
        ?>
        <!-- Jssor Slider Begin -->
        <!-- To move inline styles to css file/block, please specify a class name for each element. --> 
        <div id="slider3_container" style="position: relative; top: 0px; left: 0px; width: 1130px; height: 550px; background: #fff; overflow: hidden; ">
            <!-- Slides Container -->
            <div u="slides" style="position: absolute; left: 149px; top: 0px; width: 1010px; height: 550px; -webkit-filter: blur(0px); background-color: #fff; overflow: hidden;">
                <?php
                for ($index = 0; $index < count($this->srcContents); $index++) {
                    $this->renderSlideElement($this->tabsText[$index], $this->srcContents[$index]);
                }
                ?>
            </div>
            <div u="thumbnavigator" class="jssort13" style="left: 0px; top: 0px;">
                <!-- Thumbnail Item Skin Begin -->
                <div u="slides" style="cursor: default; top:0px; left:0px;">
                    <div u="prototype" class="p">
                        <div class=w><div u="thumbnailtemplate" class="c"></div></div>
                    </div>
                </div>
                <!-- Thumbnail Item Skin End -->
            </div>
        </div>
        <?php
    }

}
