<?php

namespace Advannova\TemplateEngine\Templates\Components;

class BodyBreadCrumbs {

    static function render($config) {
        $home = 'index.php';
        ?>
        <section id = "inner-headline">
            <div class = "container">
                <div class = "row">
                    <div class = "col-lg-12">
                        <ul class = "breadcrumb">
                            <li><a href = "<?php echo $home; ?>"><i class = "fa fa-home"></i></a><i class = "icon-angle-right"></i></li>
                            <?php
                            if (isset($config['breadcrumbs'])) {
                                foreach ($config['breadcrumbs'] as $breadcrumb) {
                                    ?>
                                    <li><a href="<?php echo $breadcrumb['link'] == '' ? "$home" : $breadcrumb['link']; ?>"><?php echo $breadcrumb['label']; ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                            <li class = "active"><?php echo $config['currentPage']; ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }

}
