<?php

namespace Advannova\TemplateEngine\Templates\Content;

require_once __DIR__ . '/../../src/common.php';

use Advannova\Common\Constants;

ob_start();
?>
<!-- Start the layout for the header -->
<div id="wrapper">
    <header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                        <!--This span elements are used for boostrap to show the strips in the collapsed button.-->
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.php"><img id="advannova-logo" src="img/advannova-logo.png" class="img-responsive"></a>
                </div>
                <!--MENU-->
                <div id="main-menu" class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <!--HOME-->
                        <li <?php if ($view['currentPage'] == "Home") { ?>class="active"<?php } ?>><a href="home.php">Home</a></li>
                        <!--SOLUTIONS-->
                        <li class="dropdown <?php if ($view['currentPage'] == "Solutions") { ?>active<?php } ?>">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Solutions <b class=" icon-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                <!--RETAIL POS-->
                                <li class="dropdown-submenu"><a href="<?php echo Constants::RETAIL_POS_HREF; ?>"><?php echo Constants::RETAIL_POS_LABEL; ?></a> 
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo Constants::COUNTERPOINT_HREF; ?>"><?php echo Constants::COUNTERPOINT_LABEL; ?></a></li>
                                        <li><a href="<?php echo Constants::LIMITLESS_POS_HREF; ?>"><?php echo Constants::LIMITLESS_POS_LABEL; ?></a></li>
                                    </ul>
                                </li>
                                <!--HOSPITALITY POS-->
                                <li class="dropdown-submenu"><a href="<?php echo Constants::HOSPITALITY_HREF; ?>"><?php echo Constants::HOSPITALITY_LABEL; ?></a> 
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo Constants::LIMITLESS_POS_HREF; ?>"><?php echo Constants::LIMITLESS_POS_LABEL; ?></a></li>
                                        <li><a href="<?php echo Constants::REST_MANAGER_HREF; ?>"><?php echo Constants::REST_MANAGER_LABEL; ?></a></li>
                                    </ul>
                                </li>
                                <!--HARDWARE-->
                                <li class="dropdown-submenu"><a href="<?php echo Constants::HARDWARE_HREF; ?>"><?php echo Constants::HARDWARE_LABEL; ?></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo Constants::HARDWARE_HREF; ?>"><?php echo Constants::NCR_HARDWARE_LABEL; ?></a></li>
                                        <li><a href="<?php echo Constants::HARDWARE_HREF; ?>"><?php echo Constants::TOUCH_DYNAMIC_LABEL; ?></a></li>
                                    </ul>
                                </li>
                                <!--CREDIT CARDS-->
                                <li><a href="<?php echo Constants::CREDIT_CARDS_HREF; ?>"><?php echo Constants::CREDIT_CARDS_LABEL; ?></a></li>
                                <!--LIMITLESS LOGISTIX-->
                                <li><a href="<?php echo Constants::LIMITLESS_LOGISTIX_HREF; ?>"><?php echo Constants::LIMITLESS_LOGISTIX_LABEL; ?></a></li>
                            </ul>
                        </li>
                        <!--FINANCING-->
                        <li class="<?php if ($view['currentPage'] == Constants::FINANCING_LABEL) { ?>active<?php } ?>">
                            <a href="<?php echo Constants::FINANCING_HREF;?>"><?php echo Constants::FINANCING_LABEL; ?></a>
                        </li>

                        <li <?php if ($view['currentPage'] == Constants::CREDIT_CARDS_LABEL) { ?>class="active"<?php } ?>><a href="<?php echo Constants::CREDIT_CARDS_HREF; ?>"><?php echo Constants::CREDIT_CARDS_LABEL; ?></a></li>
                        <li <?php if ($view['currentPage'] == Constants::SOFTWARE_DEVELPMENT_LABEL) { ?>class="active"<?php } ?>><a href="<?php echo Constants::SOFTWARE_DEVELPMENT_HREF; ?>"><?php echo Constants::SOFTWARE_DEVELPMENT_LABEL; ?></a></li>
                        <li <?php if ($view['currentPage'] == Constants::LIMITLESS_LOGISTIX_LABEL) { ?>class="active"<?php } ?>><a href="<?php echo Constants::LIMITLESS_LOGISTIX_HREF; ?>"><?php echo Constants::LIMITLESS_LOGISTIX_LABEL; ?></a></li>
                        <li <?php if ($view['currentPage'] == Constants::SUPPORT_LABEL) { ?>class="active"<?php } ?>><a href="support.php">Support</a></li>
                        <li <?php if ($view['currentPage'] == "Contact") { ?>class="active"<?php } ?>><a href="<?php echo Constants::CONTACT_US_HREF; ?>"><?php echo Constants::CONTACT_US_LABEL; ?></a></li>
                        <li id="main-menu-phone-contact"><a><i class="fa fa-phone"></i> (512) 591-7806</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
</div>
<!-- End the layout for the header -->

<!--Here comes the layout of the body content-->
<?php echo $view['body_content']; ?>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!--End of body content layout-->

<!-- Start the layout for the footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h5 class="widgetheading">Get in touch with us</h5>
                <address>
                    <strong>Advannova Inc</strong><br>
                    2851 Joe Dimaggio Blvd 29 & 30<br>
                    Round Rock, TX  78665<br>
                    <i class="fa fa-phone"></i> (512) 591-7806<br>
                    <i class="fa fa-print"></i> (512) 244-9080<br> 
                </address>
                <a href="http://www.gorspa.org"><img src="img/rspa-logo-w-bg-t-m.png" alt="Point of Sale Technology Ecosystem"></a>
                <a href="http://www.bbb.org"><img class="" src="img/bbb-logo-white-medium.png" alt="Better Business Bureau"></a>
            </div>
            <div class="col-sm-6">
                <div class="widget">
                    <h5>About Advannova</h5>
                    <p  class="text-justify">
                        At Advannova, we focus on the core features and functions that are essential to the operations of your business: your point-of-sale system. The Advannova team utilizes over 27 years of experience providing hardware, software, and services to any retail or hospitality business. We partner with NCR to help support efficiency, ease of use, and alignment with your company's operational needs.
                        We believe that technology is the Advantage to improve the customer experience through speed of service, quality, and reliability. Our entire team is focused on providing services and technology solutions to help make our customers successful.
                    </p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="align-right text-right">
                    <h5 class="widgetheading">Pages</h5>
                    <ul class="link-list">
                        <li><a href="<?php echo Constants::COUNTERPOINT_HREF; ?>"><?php echo Constants::COUNTERPOINT_LABEL; ?></a></li>
                        <li><a href="<?php echo Constants::REST_MANAGER_HREF; ?>"><?php echo Constants::REST_MANAGER_LABEL; ?></a></li>
                        <li><a href="<?php echo Constants::LIMITLESS_LOGISTIX_HREF; ?>"><?php echo Constants::LIMITLESS_LOGISTIX_LABEL; ?></a></li>
                        <li><a href="<?php echo Constants::LIMITLESS_POS_HREF; ?>"><?php echo Constants::LIMITLESS_POS_LABEL; ?></a></li>
                        <li><a href="<?php echo Constants::CREDIT_CARDS_HREF; ?>"><?php echo Constants::CREDIT_CARDS_LABEL; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="copyright">
                    <span>&copy; Advannova 2015 All right reserved.</span>
                </div>
            </div>
        </div>
    </div>
</div>
</footer>
<!-- End the layout for the footer -->

<?php
$view['body'] = ob_get_clean();
include_once __DIR__ . '/../html_structure/baselayout.php';
