<?php

namespace Advannova\TemplateEngine\Templates\Content;

/*
 * Advannova License
 * 
 */
require_once __DIR__ .'/../components/body_breadcrumbs.php';

use Advannova\TemplateEngine\Templates\Components\BodyBreadCrumbs;

ob_start();
BodyBreadCrumbs::render($view);
?>
<section id="content">
    <?php echo $view['body_content']; ?>
</section>
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/layout_header_footer.php';
