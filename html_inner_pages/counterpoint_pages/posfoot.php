<?php

namespace Advannova\WebSite\CouterPoint\POS\Footwear;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "Radiant Systems is a leading provider of retail store technology amongst footwear retailers across North America. We have customized our software to deliver specific needs of this industry by including out-of-the-box features such as three dimensional grids, robust customer loyalty programs, the ability to view history from any time, unlimited barcodes and more. Contact us today to see why CounterPoint is the perfect fit for thousands of retailers.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. CounterPoint has all the features you need to efficiently and effectively operate your business at its fullest potential.";

$featuresList = ["Three dimensional grid for inventory, ordering, and reporting in color/size/width",
    "Oversize pricing module to define and use when larger sizes need to carry a higher price",
    "Quick Reports-Create custom views of what you want to see, and how you want to view it. Add that View to a button in the POS for future reference.",
    "Manage sales, returns, orders, special orders, backorders, and layaways on the same ticket",
    "Track customers favorite colors, size, etc. with user-defined profile fields",
    "Suggest items, substitute items and link Items",
    "Robust multi-store capabilities. Easily view and transfer items from store to store, while each store data is synchronized across multiple sites",
    "View your history from any point in time-6 months, 12 months, 2 years, or 20 years.",
    "Integrated credit. Radiant hardware builds in the credit card swipe directly on the terminal. Eliminating EOD transaction reconciliation and double-entry",
    "Open-database enables you to easily pull your information out of CounterPoint into other programs such as Microsoft Excel or Adobe Acrobat",
    "Wireless Inventory Scanning-With inventory management, handhelds are instrumental in streamlining receiving, counting, ordering, and audits",
    "Modern touchscreen interface allows you to train employees in 30 minutes",
    "Enhanced security features"];

InnerPagesTemplate\renderInnerPageTemplate('Footwear', $paragraphes, $featuresList);

