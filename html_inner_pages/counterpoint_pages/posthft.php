<?php

namespace Advannova\WebSite\CouterPoint\POS\Thrift;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "CounterPoint is customizable, allowing you to configure the software to fit the needs of your business. Our software is developed to meet the specific needs of thrift and discount retailers, giving you access to unlimited barcodes, a user-friendly customer interface, and the ability to easily create donation receipts with Tax ID numbers. In addition, our award-winning hardware is designed specifically for the retail environment.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. CounterPoint has all the features you need to efficiently and effectively operate your business at its fullest potential.";

$featuresList = ["Easy to print bar codes",
"Touchscreen point of sale makes training volunteers or seasonal employees quick and easy",
"Track employee or volunteer hours",
"Print donation receipts",
"Set up special promotions and coupons",
"Modern touchscreen interface allows you to train employees in 30 minutes",
"Reduce errors at the register",
"All-in-one solutions for all-in-one support"];

InnerPagesTemplate\renderInnerPageTemplate('Thrift', $paragraphes, $featuresList);

