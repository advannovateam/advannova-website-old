<?php

namespace Advannova\WebSite\CouterPoint\POS\SpecialtyFoods;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "Specialty Food retailers can operate as a distributer, wholesaler, retailer, and caf owner-separately or simultaneously; therefore it is important the software be versatile and customizable. CounterPoint has all the ingredients Specialty Food retailers need to track their inventory, manage their vendors, and even sell their goods online.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. CounterPoint has all the features you need to efficiently and effectively operate your business at its fullest potential.";

$featuresList = ["Scale interface for weighing produce or bulk food",
"Customer facing displays for a great ROI including selling ad space to vendors or promote special events",
"Random weight barcode support for deli counter scales",
"Automated purchasing calculates the quantities to reorder based on current inventory levels, commitments, backorders, in-transit merchandise, open PO's, desired stocking levels, vendor multiples, and/or vendor minimum order requirements",
"Built-in gift cards",
"Review on-hand quantities, estimated costs, and total retail value for each item, as well as the percentage-to-total ratios for these values",
"2-second credit card authorization",
"Integrated customer loyalty programs and customer relationship management",
"EBT Support/Food Stamp tracking",
"Return to Vendors (RTVs) may be entered, reviewed, and posted to record the return of merchandise to a vendor. RTVs reduce on-hand inventory. RTVs may also be vouchered into Accounts Payable to record credits with a vendor"];

InnerPagesTemplate\renderInnerPageTemplate('Specialty Foods', $paragraphes, $featuresList, 'SpecialtyFoods');

