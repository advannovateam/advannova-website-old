<?php

namespace Advannova\WebSite\CouterPoint\POS\Museum;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "Museum retailers require technology that is versatile and highly customizable-providing tools that can accurately track a variety of inventory items, offer lightning fast checkout for high-volume periods, is easy to learn and implement, handles concessions and can even print museum and attraction tickets. Join the hundreds of museums who rely on CounterPoint to efficiently and effectively operate their businesses at their fullest potential.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. CounterPoint has all the features you need to efficiently and effectively operate your business at its fullest potential.";

$featuresList = ["Unlimited number of payment types accepted",
"Touchscreen point of sale makes training volunteers or seasonal employees quick and easy",
"Print admission tickets at the point of sale or online",
"Calculate sales commissions for each item on the sales ticket based on either the sale amount or the gross profit",
"Integration with membership systems",
"Built-in merchant services provides 2-second card processing and check authorization",
"Easily view quantities to reorder based on current inventory levels, commitments, backorders, in-transit merchandise, open PO's, desired stocking levels, vendor multiples, and/or vendor minimum order requirements"];

InnerPagesTemplate\renderInnerPageTemplate('Museum', $paragraphes, $featuresList);

