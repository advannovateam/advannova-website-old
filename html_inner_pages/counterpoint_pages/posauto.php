<?php

namespace Advannova\WebSite\CouterPoint\POS\Automotive;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "The automotive aftermarket industry needs the ability to sell and track profits for sales of merchandise and service. In addition, customer profile information is vital to successful back-to-base marketing such as notifying a customer when it's time to buy new tires or letting the hobbyist know about the latest item for their particular make and model of vehicle. Radiant Systems' feature rich and flexible business software, CounterPoint, provides all that capability and more. CounterPoint gives you the functionality you need to own your data, see it the way you want to see it, and use to drive sales, understand trends, maximize purchasing dollars and more efficiently run your business.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. Join the thousands of automotive aftermarket retailers that rely on CounterPoint to efficiently and effectively operate their businesses at their fullest potential.";

$featuresList = [
    "Ability to offer a full layaway program",
    "Data interchange allows vendor items and prices to be added/updated electronically",
    "Tax codes allow retailers to accommodate virtually any taxing scenario such as recreational vehicle tax",
    "Flexible reporting allows for analysis of inventory stock, sales, turns, profitability, and merchandising",
    "Special order capability allows tracking of Special Order items, from the customer order to receiving, allocating the inventory all the way through the process",
    "Serial number tracking provides a detailed record of each piece of merchandise, when it was received, what it cost, which customer purchased it, and when it was sold.",
    "Kit processing allows inventory items, service, and warranties to be sold as a package",
    "Embedded loyalty programs allow retailers to reward frequent shoppers and encourage repeat business",
    "Full Accounts Receivable for large customers like dealerships that need to pay on account",
    "Calculate sales commissions for each item on the sales ticket based on either the sale amount or the gross profit",
    "2-second credit card authorization"];

InnerPagesTemplate\renderInnerPageTemplate('Automotive', $paragraphes, $featuresList);

