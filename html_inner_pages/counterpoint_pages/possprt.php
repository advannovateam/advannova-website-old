<?php

namespace Advannova\WebSite\CouterPoint\POS\SportingGoods;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "As a sporting goods retailer, you need technology that not only manages your wide-range inventory of snowboards to tennis rackets, but has the gear to ensure you are running your business at its maximum efficiency. Only CounterPoint controls your inventory, manages your vendors, and can even sell your goods online. Plus, it has the functionality to track schools, team colors and team uniforms with user-defined profile fields, ensures warrantee information stays up-to-date, and has special ordering capabilities.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. Join the hundreds of sporting good retailers that rely on CounterPoint to efficiently and effectively operate their businesses at their fullest potential.";

$featuresList = ["Serial number tracking provides a detailed record of each piece of merchandise when it was received, what it cost, which customer purchased it, when it was bought, and warranty expiration. Ideal for high end fitness equipment and bicycles",
    "Kits at point of sale for bundling bikes, baskets and horns or a ski package with boots, poles, etc",
    "Alternate units for selling golf balls by the sleeve, case or each",
    "Fully integrated gift cards",
    "Calculate sales commissions for each item on the sales ticket based on either the sale amount or the gross profit",
    "Monitor the productivity of sales reps in units and dollars sold per hour",
    "Integrated customer loyalty programs and customer relationship management",
    "Special order capability so you can order team apparel or special order gear",
    "Labels allows you to create and print labels for merchandise and supports barcode printing"];

InnerPagesTemplate\renderInnerPageTemplate('Sporting Goods', $paragraphes, $featuresList, 'SportingGoods');

