<?php

namespace Advannova\WebSite\CouterPoint\POS\Specialty;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "CounterPoint is customizable, allowing you to configure the software to fit the needs of your business. Our local support teams can tailor your solution how ever you see fit. Our portfolio of clients is diverse, including battery stores, apparel retailers, franchises, zoos and aquariums, universities, museums and coffee shops.The Radiant Retail Solution helps thousands of retailers of all sizes maximize their profit opportunity with a solution that is both innovative and time-tested.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. CounterPoint has all the features you need to efficiently and effectively operate your business at its fullest potential.";

$featuresList = ["Three dimensional grid for inventory, ordering, and reporting in color/size/width",
    "Kits at point of sale for bundling items",
    "Graphical management dashboard",
    "Quick Reports-Create custom views of what you want to see, and how you want to view it. Add that View to a button in the POS for future reference",
    "Print admission tickets at the point of sale",
    "Manage sales, returns, orders, special orders, backorders, and layaways on the same ticket",
    "Track customers' favorite colors, size, etc. with user-defined profile fields",
    "Suggest items, substitute items and link Items",
    "Robust Multi-store capabilities. Easily view and transfer items from store to store, while each store data is synchronized across multiple sites",
    "View your history from any point in time-6 months, 12 months, 2 years, or 20 years.",
    "Integrated credit. Radiant hardware builds in the credit card swipe directly on the terminal. Eliminating EOD transaction reconciliation and double-entry",
    "Open-database enables you to easily pull your information out of CounterPoint into other programs such as Microsoft Excel or Adobe Acrobat",
    "Wireless Inventory Scanning-With inventory management, handhelds are instrumental in streamlining receiving, counting, ordering, and audits",
    "Modern touchscreen interface allows you to train employees in 30 minutes",
    "Enhanced Security features"];

InnerPagesTemplate\renderInnerPageTemplate('Specialty', $paragraphes, $featuresList);

