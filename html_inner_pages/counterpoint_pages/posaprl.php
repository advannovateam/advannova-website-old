<?php

namespace Advannova\WebSite\CouterPoint\POS\Apparel;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "For apparel retailers it is imperative that their inventory stays current and accurate, and that their customers are loyal. In addition to robust reporting capabilities, CounterPoints out-of-the-box features include robust Customer Relationship Management programs, which can track pertinent customer information such as purchase history and important dates, giving retailers the means to market directly to specific target audiences straight from the POS.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. Join the hundreds of apparel retailers that rely on CounterPoint to efficiently and effectively operate their businesses at their fullest potential.";

$featuresList = ["Three dimensional size grid.",
    "User definable fields to track X, Y, Z.",
    "Suggested items, substitute items and linked Items.",
    "Special order capability.",
    "Group markdowns, quantity pricing, coupons, multi buy items, bulk discount.",
    "Calculate sales commissions for each item on the sales ticket based on either the sale amount or the gross profit.",
    "Monitor the productivity of sales reps in units and dollars sold per hour.",
    "Review on-hand quantities, estimated costs, and total retail value for each item, as well as the percentage-to-total ratios for these values.",
    "Easily view quantities to reorder based on current inventory levels, commitments, backorders, in-transit merchandise, open POS, desired stocking levels, vendor multiples, and/or vendor minimum order requirements.",
    "The Merchandise Analysis feature lets you see the current state and the past performance of your inventory, giving you the data to determine when to take advantage of opportunistic buys and which items to mark down.",
    "Built-in gift cards",
    "Ability to offer a full layaway program",
    "2-second credit card authorization for busy holiday season",
    "Integrated customer loyalty programs and customer relationship management"];

InnerPagesTemplate\renderInnerPageTemplate('Apparel', $paragraphes, $featuresList);