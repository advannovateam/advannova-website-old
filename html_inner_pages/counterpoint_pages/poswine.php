<?php

namespace Advannova\WebSite\CouterPoint\POS\LiquorAndWine;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "Retailers in this segment rely on CounterPoint to provide an accurate, real-time inventory picture, which reduces shrinkage, minimizes overbuying, and ensures that popular items are stocked appropriately. Easily keep up with brands, distributers, special orders, vendors, house accounts, and more. Only CounterPoint has the features that give retailers control over every aspect of their business-tracking their inventory, managing vendors and even selling goods online!";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. Join the hundreds of beverage retailers that rely on CounterPoint to efficiently and effectively operate their businesses at their fullest potential.";

$featuresList = ["Integrated customer loyalty programs and customer relationship management",
"Merchandise is priced accurately and automatically, based on list price, quantity breaks, customer discounts, a markup on cost, or a desired margin",
"Customer facing displays for a great ROI including selling ad space to vendors or promote special events",
"Setup House Accounts for loyal customers. Encrypted credit card information may be retained for each customer and may be retrieved automatically when paying for a purchase",
"House customers are open-item type accounts. Customer terms and prompt payment discounts may be based on a number of days (\"net 30\" or \"2% 10, net 30\"), or proximo terms based on a specific day of the month (\"2% 10th, net 30th\")",
"Automated purchasing calculates the quantities to reorder based on current inventory levels, commitments, backorders, in-transit merchandise, open PO's, desired stocking levels, vendor multiples, and/or vendor minimum order requirements",
"Built-in gift cards",
"Items can be priced and sold by using the stocking unit or up to five alternate selling units. For example, you may stock wine bottles by the each, and sell them at one price by the case and at another price by the half case",
"Track deposits for items such as taps and kegs",
"2-second credit card authorization",
"Return to Vendors (RTVs) may be entered, reviewed, and posted to record the return of merchandise to a vendor. RTVs reduce on-hand inventory. RTVs may also be vouchered into Accounts Payable to record credits with a vendor"];

InnerPagesTemplate\renderInnerPageTemplate('Liquor and Wine', $paragraphes, $featuresList, 'Wine');

