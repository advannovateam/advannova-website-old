<?php

namespace Advannova\WebSite\CouterPoint\POS\GreenIndustry;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "Retailers in the Green Industry consist of business from lawn and garden centers to nurseries. Green Industry retailers rely on NCR Counterpoint to accurately track their live and continuously changing inventory, manage their diverse venders, and streamline their business operations. With its offline capabilities, ease of use, and quick ROI, NCR Counterpoint has grown to be a household name amongst hundreds of retailers in the Green community.";
$paragraphes[] = "NCR Counterpoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store ? NCR Counterpoint includes everything you need to streamline your business and boost your bottom line. NCR Counterpoint has all the features you need to efficiently and effectively operate your business at its fullest potential.";
$paragraphes[] = "Reporting is essential to help you make better decisions in the future. What are your high-profit items and categories? Which products do you need to buy? Which items are slow-movers? One of our favorite reports is our Merchandise Analysis Report which allows you to slice and dice inventory and sales history data based on parameters that are important to you. We know our developers don?t always know what you look for in a report, so we allow you to customize not only the parameters, but also the fields displayed on the report. After creating a report, email it to a coworker or drop it into Microsoft Excel for further analysis.";
$paragraphes[] = "<b>Customer Segmentation using NCR Customer Connect</b>";
$paragraphes[] = "Using the segmentation tool in our integrated email marketing tool, group your customers based on information from their customer profile (gender, birthday, etc.) or purchase history. Use these lists to send targeted emails or direct mailings to specific groups of customers to drive traffic. For example, you can set up an email to automatically remind customers to come back in for fertilizer 3 months after they purchase a particular tree.";
$paragraphes[] = "Show your customers you know and care about them by communicating in a targeted way. A customer who attended your orchid class, may be interested in attending your upcoming Repotting Basics course. The customer who purchased lilies last year will likely be interested again this year. You have a lot of data about your customers, now you can use that data to drive traffic and increase customer loyalty!";
$paragraphes[] = "<i>\"NCR Customer Connect is our sole email marketing platform. I love that the system ties everything together. I don't have to import and manage email lists. That has been a huge help.\"</i>";
$paragraphes[] = "<p class='text-right'><i>- Karen Thacker, VP, Operations, Altum's</i></p>";

$featuresList = ["Alternate units give the ability to sell a plant or a flat of plants",
    "Unlimited bar codes for tracking the same item sold from many vendors like seed packets",
    "Item images to verify the correct item is being sold",
    "Quotes for landscapers who need a way to track products by client",
    "2-second credit card authorization for busy holiday weekend",
    "Integrated customer loyalty programs and customer relationship management",
    "Built-in Gift Card functionality",
    "Waterproof and fade-resistant labels for outdoor inventory",
    "Kits at point of sale for bundling items",
    "Retail-hardened Hardware that is built to withstand extensive outdoor elements such as direct sunlight, dirt and soil"];

InnerPagesTemplate\renderInnerPageTemplate('Green Industry', $paragraphes, $featuresList, 'Green');

