<?php

namespace Advannova\WebSite\CouterPoint\POS\Franchise;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "Whether you're a single or multi-site franchise, Radiant Systems will help you drive top-line growth by delivering speed, reliability and consistency in you operation while providing great visibility to more effectively managing your bottom line. As a leading point of sale (POS) provider, Radiant Systems delivers technology solutions built specifically for franchise operators, both corporate and franchisee. The solution features advanced, easy-to-use point of sale inventory management, and labor scheduling as well as real-time reporting.";

$featuresList = ["Implement a custom gift card program across all franchisees (SVC cards).",
    "Automatically generate end of day reports for each franchisee and set the reports to send directly to the corporate office.",
    "Leverage quantity breaks from vendors with a centralized purchasing perspective",
    "Variety of deploy models for franchise locations.",
    "Develop a system that new franchisees can easily manipulate.",
    "Robust reporting capabilities to measure profitability across all locations.",
    "Franchisees can integrate own financials.",
    " 2-second credit card authorization for busy holiday season.",
    "Track, share and transfer merchandise across all franchise locations."];

InnerPagesTemplate\renderInnerPageTemplate('Franchise', $paragraphes, $featuresList);
