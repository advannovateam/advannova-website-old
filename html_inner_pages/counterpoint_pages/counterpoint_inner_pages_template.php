<?php

namespace Advannova\WebSite\CouterPoint\InnerPagesTemplate;

/*
 * Advannova License
 * 
 */

function renderInnerPageTemplate($keyWordArea, $paragraphes, $featuresList, $imageName = '') {
    $imageName = $imageName == '' ? $keyWordArea : $imageName;
    ?>
    <h4 class="aligncenter"><?php echo $keyWordArea; ?> POS Software</h4>
    <p class="text-info aligncenter">Click the image below to <b>Download</b> the brochure.</p>
    <div class="aligncenter"><a href="downloadable/pdfs/counterpoint/SR_NCR-Counterpoint-Overview.pdf" target="_blank"><img src="img/counterpoint/HP-<?php echo $imageName; ?>.jpg"></a></div>
    <div class="text-justify margintop40">
        <h5>The Radiant Retail Solution for <?php echo $keyWordArea; ?> Retailers</h5>
        <?php
        foreach ($paragraphes as $p) {
            ?>
            <p><?php echo $p; ?></p>
            <?php
        }
        ?>
        <h5>Features that are beneficial to <?php echo $keyWordArea; ?> Retailers:</h5>
        <ul>
            <?php
            foreach ($featuresList as $li) {
                ?>
                <li><?php echo $li; ?></li>
                <?php
            }
            ?>
        </ul>
    </div>
    <?php
}
