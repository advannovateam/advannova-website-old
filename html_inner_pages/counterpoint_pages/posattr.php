<?php

namespace Advannova\WebSite\CouterPoint\POS\Attractions;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "Attractions, such as zoos and aquariums, need a central center to manage the sales at multiple revenue centers throughout the facility. Not only do they handle admissions, but have gift shops, concessions, rentals and membership programs. CounterPoint is able to operate every function of the park from a single system, plus track memberships, customers, groups and more. CounterPoint's complete solution, extensive reporting capabilities and robust features ensure some of the most recognizable and established parks in North America are operating at their fullest potential.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. CounterPoint has all the features you need to efficiently and effectively operate your business at its fullest potential.";

$featuresList = [
    "Unlimited number of payment types accepted.",
    "Touchscreen point of sale makes training volunteers or seasonal employees quick and easy.",
    "Manage all aspects of the Retail environment from brick and mortar to integrated online store.",
    "Print admission tickets at the point of sale or online.",
    "Built-in merchant services provides 2-second card processing and check authorization.",
    "Easily view quantities to reorder based on current inventory levels, commitments, backorders, in-transit merchandise, open PO's, desired stocking levels, vendor multiples, and/or vendor minimum order requirements.",
    "Offline ticket entry gives Attractions the ability to seamlessly operate at an off-site event, such as a fundraiser, and utilize portable kiosks.",
    "Rolling customer display at POS created advertisements for future events.",
    "Keyword lookups simplifies finding items or customers.",
    "Manage concessions including cafeteria and coffee in one, integrated solution.",
    "Serial number tracking provides a detailed record of each piece of merchandise-when it was received, what it cost, which customer purchased it, and when it was sold. Ideal for high-end inventory.",
    "Manage reservations and scheduling of events for schools, groups and individuals online and on site."];

InnerPagesTemplate\renderInnerPageTemplate('Attractions', $paragraphes, $featuresList);

