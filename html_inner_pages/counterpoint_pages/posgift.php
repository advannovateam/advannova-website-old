<?php

namespace Advannova\WebSite\CouterPoint\POS\GiftRetail;

/*
 * Advannova License
 * 
 */
require_once 'counterpoint_inner_pages_template.php';

use Advannova\WebSite\CouterPoint\InnerPagesTemplate;

$paragraphes[] = "Only Radiant Systems offers a seamless, end-to-end solution for specialty gift retailers. Our one-stop-shop solution includes the leading retail management software, CounterPoint, robust customer programs, built-in gift cards, integrated ecommerce, merchant services and so much more.";
$paragraphes[] = "CounterPoint includes a robust inventory management system, touchscreen point of sale (POS) ticket entry, built-in customer loyalty programs, automated purchasing and impressive reporting capabilities. Instant inventory updates let you know what's in stock, on order, and in transit. Track your customers, control your purchasing, retain detailed sales history, or open an online store-CounterPoint includes everything you need to streamline your business and boost your bottom line. Join the hundreds of gift stores that rely on CounterPoint to efficiently and effectively operate their businesses at their fullest potential.";

$featuresList = ["Kits at point of sale for bundling items",
    "Built-in Gift Cards",
    "Automatically calculate and create restocking orders",
    "Expand your business to the web with minimum effort and expense",
    "Graphical management dashboard",
    "Special order capability",
    "Review on-hand quantities, estimated costs, and total retail value for each item, as well as the percentage-to-total ratios for these value",
    "Easily view quantities to reorder based on current inventory levels, commitments, backorders, in-transit merchandise, open POs, desired stocking levels, vendor multiples, and/or vendor minimum order requirements",
    "Integrated merchant services provides 2-second card processing and check authorization"];

InnerPagesTemplate\renderInnerPageTemplate('Gift', $paragraphes, $featuresList);

