<?php

namespace Advannova\WebSite\CouterPoint;

/*
 * Advannova License
 * 
 */
require_once './templates/components/slider.php';
require_once './templates/components/tab_slider.php';
require_once './src/common.php';

use Advannova\Common\Constants;
use Advannova\TemplateEngine\Templates\Components\SliderImage;
use Advannova\TemplateEngine\Templates\Components\SliderTab;

/*
 * Setting up template's configurations
 */
$view['currentPage'] = Constants::COUNTERPOINT_LABEL;
$view['breadcrumbs'] = [['label' => 'Solutions', 'link' => ''], ['label' => Constants::RETAIL_POS_LABEL, 'link' => Constants::RETAIL_POS_HREF]];

/*
 * Body content starts here
 */
ob_start();
//Slider Imgage
$sliderImage = new SliderImage();
$sliderImage->addSlider('img/counterpoint/slider/large-nro-on-lawn.jpg', 'eCommerce', 'Seamless integration with your retail management system means less administration.');
$sliderImage->addSlider('img/counterpoint/slider/cp-dashboard-garden.jpg', 'Hardware', 'Although not required to use NCR Counterpoint, our POS terminals help your staff deliver exceptional service.');
$sliderImage->addSlider('img/counterpoint/slider/counterpoint_mobile.jpg', 'Mobile Point of Sale', 'Get out from behind the counter and ring up sales anywhere on the floor.');
$sliderImage->render();
?>

<div class="container margintop40">
    <img src="img/counterpoint/NCR Logo 2016.jpg">
    <?php
    //Jssor tab slider
    $sliderTab = new SliderTab();
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posspec.php', 'Specialty Retail');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posaprl.php', 'Apparel');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posattr.php', 'Attractions');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posauto.php', 'Automotive');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posfoot.php', 'Footwear');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posfran.php', 'Franchise');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posgift.php', 'Gift Retail');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posgren.php', 'Green Industry');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posmusm.php', 'Museums');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posspfd.php', 'Specialty Foods');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/possprt.php', 'Sporting Goods');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/posthft.php', 'Thrift');
    $sliderTab->addSliderTab('html_inner_pages/counterpoint_pages/poswine.php', 'Wine/Liquor');
    $sliderTab->render($view, 'js/tab_slider_jssor/counterpoint_tab_slider.js');
    ?>
</div>
<!--Body content ends here-->
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
