<?php

namespace Advannova\WebSite\Support;

/*
 * Advannova License
 * 
 */
/*
 * Setting up template's configurations
 */
require_once './src/common.php';

use Advannova\Common\Constants;

$view['currentPage'] = Constants::SUPPORT_LABEL;

ob_start();
?>
<div class="container margintop30">
    <div class="row ">
        <div class="col-lg-5 col-lg-push-1">
            <div class="row">
                <div class="col-lg-5"><a href="<?php echo Constants::SUPPORT_TICKET_HREF ?>"><img class="center-block" src="img/support/create-ticket.png"></a></div>
                <div class="col-lg-5"><a href="<?php echo Constants::SUPPORT_TICKET_HREF ?>"><h4 class="margintop40"><?php echo Constants::SUPPORT_TICKET_LABEL ?></h4></a></div>
            </div>
            <div class="row">
                <div class="col-lg-5"><img class="center-block margintop20" src="img/support/voice-support.png"></div>
                <div class="col-lg-7">
                    <h4>Phone Support:</h4><h5>(512) 591-7806</h5>
                    <h4>Emergency Support:</h4><h5>(512) 579-9576</h5>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-lg-push-1">
            <a href="<?php echo Constants::SUPPORT_TICKET_HREF ?>"><img class="img-responsive center-block image-nextlink" src="img/support/support.jpg" title="Click to create a ticket"></a>
        </div>
    </div>
</div>
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
