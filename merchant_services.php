<?php

namespace Advannova\WebSite\MerchantServices;

/*
 * Advannova License
 * 
 */
/*
 * Setting up template's configurations
 */
$view['currentPage'] = 'Merchant Services';

ob_start();
?>
<!--Here goes the body content-->
<?php
$view['body_content'] = ob_get_clean();
include_once __DIR__ . '/templates/content/layout_breadcrumbs.php';
